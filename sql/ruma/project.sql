-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2019 at 07:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `active_workdesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `project_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_type` int(11) DEFAULT NULL,
  `project_category` int(11) DEFAULT NULL,
  `project_sub_category` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `important_links` text COLLATE utf8_unicode_ci,
  `atachement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `title`, `project_type`, `project_category`, `project_sub_category`, `description`, `important_links`, `atachement`, `client_id`) VALUES
(1, 'Project1', 1, 13, 15, 'Project-1 description', 'www.facebook.com', 'project_1.jpg', 1),
(2, 'project2', 2, 14, 14, 'Project description', NULL, 'project_2.jpg', 1),
(3, 'Project3', 2, 13, 16, 'Project 3', NULL, 'project_3.jpg', 1),
(4, 'project 4', 3, 14, 18, 'Project 4 description', NULL, 'project_4.jpg', 1),
(5, 'Project 5', 1, 14, 19, 'Project 5 description', NULL, 'project_5.jpg', 1),
(6, 'project 6', 2, 14, 16, 'Project 6 description', NULL, 'project_6.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
