<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">
		<!--Page Title-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<div id="page-title">
			<h1 class="page-header text-overflow"><?= translate('add_client')?></h1>

		</div>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End page title-->
		<!--Breadcrumb-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="#"><?= translate('home')?></a></li>
			<li><a href="#"><?= translate('clients')?></a></li>
			<li><a href="#"><?= translate('add_client')?></a></li>

		</ol>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End breadcrumb-->
	</div>
	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="panel">
			<?php if (!empty($success_alert)): ?>
				<div class="alert alert-success" id="success_alert" style="display: block">
	                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
	                <?=$success_alert?>
	            </div>
			<?php endif ?>
			<?php if (!empty($danger_alert)): ?>
				<div class="alert alert-danger" id="danger_alert" style="display: block">
	                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>

	                <?=$danger_alert?>
	                 <?=validation_errors()?>
	            </div>
			<?php endif ?>
	    	<?php if (!empty(validation_errors())): ?>
                <div class="widget" id="profile_error">
                    <div style="border-bottom: 1px solid #e6e6e6;">
                        <div class="card-title" style="padding: 0.5rem 1.5rem; color: #fcfcfc; background-color: #de1b1b; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem;">You <b>Must Provide</b> the Information(s) bellow</div>
                        <div class="card-body" style="padding: 0.5rem 1.5rem; background: rgba(222, 27, 27, 0.10);">
                            <style>
                                #profile_error p {
                                    color: #DE1B1B !important; margin: 0px !important; font-size: 12px !important;
                                }
                            </style>
                            <?= validation_errors();?>
                        </div>
                    </div>
                </div>
            <?php
                endif;
            ?>

		    <div class="panel-heading">
		        <h3 class="panel-title"><?= translate('add_new_client_info')?></h3>
		    </div>
		    <div class="panel-body">
	    		<form class="form-horizontal" id="manage_details_form" method="POST" action="<?=base_url()?>admin/clients/add_client/do_add">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="fname"><b><?= translate('first_name')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" value="<?php if(!empty($form_contents)){echo $form_contents['fname'];}?>" name="fname" placeholder="<?= translate('first_name')?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="lname"><b><?= translate('last_name')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" value="<?php if(!empty($form_contents)){echo $form_contents['lname'];}?>" name="lname" placeholder="<?= translate('last_name')?>" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="email"><b><?= translate('email')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" value="<?php if(!empty($form_contents)){echo $form_contents['email'];}?>" name="email" placeholder="<?= translate('email')?>" required="">
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-3 control-label" for="mobile"><b><?= translate('mobile')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="number" value="<?php if(!empty($form_contents)){echo $form_contents['mobile'];}?>" class="form-control" name="mobile"  placeholder=" <?= translate('mobile_no.')?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="password"><b><?= translate('password')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="password"  class="form-control" name="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="cpassword"><b><?= translate('confirm_password')?> <span class="text-danger">*</span></b></label>
						<div class="col-sm-8">
							<input type="password" class="form-control" name="cpassword">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-8 text-right">
							<button type="submit" class="btn btn-primary btn-sm btn-labeled fa fa-save"><?=translate('add_client')?></button>
						</div>
					</div>
				</form>
		    </div>
		</div>
	</div>
</div>
<script>
	setTimeout(function() {
	    $('#success_alert').fadeOut('fast');
	    $('#danger_alert').fadeOut('fast');
	}, 5000); // <-- time in milliseconds
</script>
