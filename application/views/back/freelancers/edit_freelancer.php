

<?php
	$members = array();
	if ($member_type == "Free") {
		$member = $get_free_freelancer_by_id;
	}
	elseif ($member_type == "Premium") {
		$member = $get_premium_freelancer_by_id;
	}
	foreach ($member as $value) {
		$image = json_decode($value->profile_image, true);
		$education_and_career = json_decode($value->education_and_career, true);
		$basic_info = json_decode($value->basic_info, true);
		$language = json_decode($value->language, true);
		$permanent_address = json_decode($value->permanent_address, true);
	}
?>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">
		<!--Page Title-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<div id="page-title">
			<h1 class="page-header text-overflow"><?php echo translate('freelancers')?></h1>
			<!--Searchbox-->
			<div class="searchbox">
				<div class="pull-right">
					<a href="<?=base_url()?>admin/freelancers/<?=$parameter?>/view_freelancer/<?=$value->freelancer_id?>" class="btn btn-danger btn-sm btn-labeled fa fa-step-backward" type="submit"><?php echo translate('go_back')?></a>
				</div>
			</div>
		</div>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End page title-->
		<!--Breadcrumb-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="#"><?php echo translate('home')?></a></li>
			<li><a href="#"><?php echo translate('freelancers')?></a></li>
			<li><a href="#"><?=translate($member_type)?> <?php echo translate('freelancers')?></a></li>
			<li class="active"><?php echo translate('freelancer_details')?></li>
		</ol>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End breadcrumb-->
	</div>
	<!--Page content-->
                <!--===================================================-->
                <div id="page-content">
					<div class="fixed-fluid">
						<?php
							include_once "left_panel.php";
							include_once "edit_info.php";
						?>
					</div>					
                </div>
                <!--===================================================-->
                <!--End page content-->
</div>

<!--Default Bootstrap Modal-->
<!--===================================================-->
<div class="modal fade" id="package_modal" role="dialog" tabindex="-1" aria-labelledby="package_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('package_information')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body" id="package_modal_body">
            	
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="block_modal" role="dialog" tabindex="-1" aria-labelledby="block_modal" aria-hidden="true">
    <div class="modal-dialog" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('confirm_your_action')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body">
            	<p><?php echo translate('are_you_sure_you_want_to')?> "<b id="block_type"></b>" <?php echo translate('this_user?')?>?</p>
            	<div class="text-right">
            		<input type="hidden" id="freelancer_id" name="freelancer_id" value="">
            		<button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close"><?php echo translate('close')?></button>
                	<button class="btn btn-primary btn-sm" id="block_status" value=""><?php echo translate('confirm')?></button>
            	</div>
            </div>
        </div>
    </div>
</div>
<!--===================================================-->
<!--End Default Bootstrap Modal-->
<script src="<?=base_url()?>template/front/js/jquery.inputmask.bundle.min.js"></script>
<script>
    $(document).ready(function(){
        $(".height_mask").inputmask({
            mask: "9.99",
            greedy: false,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            }
        });
    });
</script>
<script>
    $(".present_country_f_edit").change(function(){
        var country_id = $(".present_country_f_edit").val();
        if (country_id == "") {
            $(".present_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".present_state_f_edit").html(data);
                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".present_state_f_edit").change(function(){
        var state_id = $(".present_state_f_edit").val();
        if (state_id == "") {
            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".present_city_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });

    $(".permanent_country_f_edit").change(function(){
        var country_id = $(".permanent_country_f_edit").val();
        if (country_id == "") {
            $(".permanent_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".permanent_state_f_edit").html(data);
                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".permanent_state_f_edit").change(function(){
        var state_id = $(".permanent_state_f_edit").val();
        if (state_id == "") {
            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".permanent_city_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".prefered_country_f_edit").change(function(){
        var country_id = $(".prefered_country_f_edit").val();
        if (country_id == "") {
            $(".prefered_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".prefered_state_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });

</script>
<script>
	function view_package(id){
		$.ajax({
		    url: "<?=base_url()?>admin/freelancer_package_modal/"+id,
		    success: function(response) {
				$("#package_modal_body").html(response);
		    },
			fail: function (error) {
			    alert(error);
			}
		});
	}
</script>
<script>
	function block(status, member_id){
	    $("#block_status").val(status);
	    if (status == 'yes') {
	    	$("#block_type").html("<?php echo translate('unblock')?>");
	    }
	    if (status == 'no') {
			$("#block_type").html("<?php echo translate('block')?>");
	    }
	    $("#freelancer_id").val(member_id);
	}

	$("#block_status").click(function(){
    	$.ajax({
		    url: "<?=base_url()?>admin/freelancer_block/"+$("#block_status").val()+"/"+$("#freelancer_id").val(),
		    success: function(response) {
		    	// alert(response);
				window.location.href = "<?=base_url()?>admin/freelancers/<?=$parameter?>";
		    },
			fail: function (error) {
			    alert(error);
			}
		});
    })
</script>