<!-- Alert for Ajax Profile Edit Section -->
<div class="col-lg-3 col-md-4" id="ajax_alert" style="display: none; position: fixed; top: 15px; right: 0; z-index: 9999">
    <div class="alert alert-success fade show" role="alert">
        <?php echo translate('data_added_successfully!')?>
    </div>
</div>
<!-- Alert for Ajax Profile Edit Section -->
<!-- Alert for Validating Ajax Profile Edit Section -->
<div class="col-lg-3 col-md-4" id="ajax_validation_alert" style="display: none; position: fixed; top: 15px; right: 0; z-index: 9999">
    <div class="alert alert-danger fade show" role="alert">
        </button>
        <span id="validation_info"></span>
    </div>
</div>
<!-- Alert for Validating Ajax Profile Edit Section -->

<div class="card-title">
    <h3 class="heading heading-6 strong-500">
        <b><?php echo translate('projects')?></b>
    </h3>
    <div class="pull-right">
	    <button type="button" class="btn btn-base-1 btn-sm btn-shadow " onclick="add_project()">
	    	<i class="ion-plus"> </i><?php echo translate('add_new_projects')?>
	    </button>
	</div>
</div>
<!-- date('m/d/Y H:i:s', 1299446702); -->
<div class="card-body " style="border-bottom: 1px solid rgba(0, 0, 0, 0.05);">
	<div class="project_list">
		<div id="result">
        <!-- Loads List Data with Ajax Pagination -->
	    </div>
	    <div id="pagination" class="pt-2" style="float: right;">
	        <!-- Loads Ajax Pagination Links -->
	    </div>
	</div>
	<div class="project_add">
	     <form class="form-default"  role="form" enctype="multipart/form-data" method="post" id="add_project">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('project_title')?></label>
                        <input type="text" class="form-control no-resize" name="title" value="" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('project_type')?></label>
                         <select name="project_type" class="form-control form-control-sm selectpicker">
                            <option value=""><?php echo translate('choose_one') ?></option>
                            <?php 
                                $projects_data = $this->db->get('project_type')->result(); 
                                foreach ($projects_data as $projects) { ?>
                                     <option value="<?php echo $projects->project_type_id ?>"><?php echo $projects->name; ?></option>
                                <?php }
                            ?>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                    <div class="form-group has-feedback">
            			<label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('project_category')?></label>
            			<select name="project_category" onchange="project_sub_category_choose(this.value)"class="form-control form-control-sm selectpicker ">
                            <option value=""><?php echo translate('choose_one') ?></option>
                            <?php 
                                $project_category_data = $this->db->get_where("project_category",array('parent'=>0))->result();
                                foreach ($project_category_data as $project_category) { ?>
                                     <option value="<?php echo $project_category->project_category_id ?>"><?php echo $project_category->title; ?></option>
                                <?php }
                            ?>
                        </select>
                        <div class="help-block with-errors"></div>
            		</div>
            	</div>
            	<div class="col-md-6">
                    <div class="form-group has-feedback">
                        <label for="" class="text-uppercase"><?php echo translate('project_sub_category')?></label>

                        <select class="form-control form-control-sm selectpicker s_project_sub_category" name="project_sub_category">
                            <option value=""><?php echo translate('choose_a_project_category_first')?></option>
                        </select>
                        <div class="help-block with-errors">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <div class="form-group has-feedback">
                        <label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('description')?></label>
                        <textarea class="form-control textarea" name="description" id="description"></textarea> 
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <label for="attachement" class="text-uppercase c-gray-light"><?php echo translate('attachement')?></label>
                        <input type="file" multiple class="form-control no-resize" name="attachement[]" value="">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <span id="more_links">
                 <div class="row" >
                    <div class="col-sm-9">
                         <div class="form-group has-feedback">
                            <label for="important_link" class="text-uppercase c-gray-light"><?php echo translate('important_link')?></label>
                            <input type="text" class="form-control no-resize" name="important_link[]" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                         <div class="form-group has-feedback">
                            <label></label>
                            <button type="button" class="btn btn-base-1 btn-sm btn-shadow "id="add_more_link_btn">
                                    <i class="ion-plus"> </i><?php echo translate('add_new_link')?>
                                </button>
                         </div>
                    </div>
                </div>
            </span>
               
             <div class="row">
                <div class="col-md-12">
                    <div class="row text-center">
                        <div class="col-3">
                            <button type="button" class="btn btn-base-1 btn-rounded btn-sm z-depth-2-bottom btn-icon-only" style="width: 100%" onclick="save_section('add_project')"><?php echo translate('submit')?></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



<style>
    .modal-dialog.modal-md {
        max-width: 500px !important; 
        margin-top: 30vh;
    }
</style>           
</section>
<script>
    $(document).ready(function(){
        filter_my_projects('0');
    });

    function filter_my_projects(page){      
        var form = $('#filter_form');
        //var url = form.attr('action')+page+'/';
        var url = '<?php echo base_url(); ?>home/ajax_projects_list/'+page;
        var place = $('#result');
        var formdata = false;
        if (window.FormData){
            formdata = new FormData(form[0]);
        }
        $.ajax({
            url: url, // form action url
            type: 'POST', // form submit method get/post
            dataType: 'html', // request type html/json/xml
            data: formdata ? formdata : form.serialize(), // serialize form data 
            cache       : false,
            contentType : false,
            processData : false,
            beforeSend: function() {
                place.html("");
                place.html("<div class='text-center pt-5 pb-5' id='payment_loader'><i class='fa fa-refresh fa-5x fa-spin'></i><p>Please Wait...</p></div>").fadeIn(); 
                // change submit button text
            },
            success: function(data) {
                setTimeout(function(){
                    place.html(data); // fade in response data
                }, 20);
                setTimeout(function(){
                    place.fadeIn(); // fade in response data
                }, 30);
            },
            error: function(e) {
                console.log(e)
            }
        });
        
    }
</script>

<script type="text/javascript">
	$('.project_add').hide();
	function add_project()
    {
         $('.project_list').hide();
         $('.project_add').show();
    }
</script>

<script type="text/javascript">
	function project_sub_category_choose(id) {

		if(id != null){
			$.ajax({
	            url: '<?php echo base_url();?>home/get_project_sub_category_by_project_category/' + id ,
	            success: function(response)
	            {
	                jQuery('.s_project_sub_category').html(response);
	            }
        	});
		}
    }
</script>

<script type="text/javascript">

    $("#add_more_link_btn").click(function(){
        $("#more_links").append(''
            +'<div class="row">' 
            +'<div class="col-sm-9"  id="more_links">'
            +'         <div class="form-group has-feedback">'
            +'            <input type="text" class="form-control no-resize" name="title" value="">'
            +'        </div>'
            +'     </div>'
            +'     <div class="col-sm-3">'
            +'         <div class="form-group has-feedback">'
            +'             <button type="button" class="btn btn-danger btn-sm btn-icon-only btn-shadow mb-1 rmc" > <i class="fa fa-trash"></i>'
            +'            </button>'
            +'         </div>'
            +'    </div>'
            +'</div>'
        );
    });
    $('body').on('click', '.rmc', function(){
        $(this).parent().parent().parent().remove();
    });

    function save_section(section)
    {
        //alert(section);
       var form = $("#add_project");
        var form_data = new FormData(form[0]);
        var prep_data = form_data ? form_data : form.serialize();
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>home/client_profile/"+section,
            cache: false,
            //data: $('#form_'+section).serialize(),
            data : prep_data ,
            processData: false,
            contentType: false,
            success: function(response) {
                // alert($('#form_'+section).serialize());
                if (IsJsonString(response)) {

                    var JSONArray = $.parseJSON(response);
                    var html = "";
                    $.each(JSONArray, function(row, fields) {
                        // alert(fields['ajax_error']);
                        html = fields['ajax_error'];
                    });
                    $('#validation_info').html(html);
                    $('#ajax_validation_alert').show();
                    $('.project_add').hide();
                    $('.project_list').show();
                    setTimeout(function() {
                        $('#ajax_validation_alert').fadeOut('fast');
                    }, 5000); // <-- time in milliseconds
                }
                else{
                    // Loading the specific Section Area
                    // alert('FALSE');
                    $('#section_'+section).html(response);
                    $('#ajax_alert').show();
                    setTimeout(function() {
                        $('#ajax_alert').fadeOut('fast');
                    }, 5000); // <-- time in milliseconds
                }

            },
            fail: function (error) {
                alert(error);
            }
        });
    }

</script>

